package com.wiesoftware.spine.ui.home.menus.podcasts.addrss.entercode

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.wiesoftware.spine.R
import com.wiesoftware.spine.RuntimeLocaleChanger
import com.wiesoftware.spine.data.repo.HomeRepositry
import com.wiesoftware.spine.databinding.ActivityEnterCodeBinding
import com.wiesoftware.spine.ui.home.menus.podcasts.addpodcasts.AddPodcastActivity
import com.wiesoftware.spine.util.toast
import kotlinx.coroutines.launch
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class EnterCodeActivity : AppCompatActivity(),KodeinAware, EnterCodeEventListener {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base?.let { RuntimeLocaleChanger.wrapContext(it) })
    }

    override val kodein by kodein()
    lateinit var binding: ActivityEnterCodeBinding
    val homeRepositry: HomeRepositry by instance()

    var email=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_enter_code)
        val viewmodel=ViewModelProvider(this).get(EnterCodeViewmodel::class.java)
        binding.viewmodel=viewmodel
        viewmodel.enterCodeEventListener=this
        homeRepositry.getUser().observe(this, Observer { user->
            email=user.email!!
            sendCodeOnEmail()
        })

    }

    private fun sendCodeOnEmail() {
        lifecycleScope.launch {
            try {
                val res=homeRepositry.sendVerificationCodeOnEmail(email)
                if (res.status){
                    "Otp sent successfully. ${res.otp}".toast(this@EnterCodeActivity)
                }
            }catch (e:Exception){
                e.printStackTrace()
            }
        }
    }

    override fun onBack() {
        onBackPressed()
    }

    override fun onNext(code: String) {

        lifecycleScope.launch {
            try {
                val res=homeRepositry.verifyEmailVerificationCode(email,code)
                if (res.status){
                    "Verification success.".toast(this@EnterCodeActivity)
                    startActivity(Intent(this@EnterCodeActivity,AddPodcastActivity::class.java))
                }else{
                    "Verification failed.".toast(this@EnterCodeActivity)
                }
            }catch (e: Exception){
                e.printStackTrace()
            }
        }

    }


    override fun onResendCode() {
        sendCodeOnEmail()
    }
}