package com.wiesoftware.spine.ui.home.menus.events.preview_event

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.wiesoftware.spine.R
import com.wiesoftware.spine.RuntimeLocaleChanger
import com.wiesoftware.spine.data.net.reponses.EventsRecord
import com.wiesoftware.spine.data.repo.HomeRepositry
import com.wiesoftware.spine.databinding.ActivityPreviewEventBinding
import com.wiesoftware.spine.ui.home.menus.events.B_IMG_URL
import com.wiesoftware.spine.ui.home.menus.events.EVE_RECORD
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class PreviewEventActivity : AppCompatActivity(),KodeinAware, PreviewEventsEventListener {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base?.let { RuntimeLocaleChanger.wrapContext(it) })
    }

    override val kodein by kodein()
    val factory: PreviewEventViewmodelFactory by instance()
    val homeRepositry:  HomeRepositry by instance()
    lateinit var  binding: ActivityPreviewEventBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_preview_event)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_preview_event)
        val viewmodel=ViewModelProvider(this,factory).get(PreviewEventViewmodel::class.java)
        binding.viewmodel=viewmodel
        viewmodel.previewEventsEventListener=this
        viewmodel.getUser().observe(this,{

            binding.textView122.text=it.name.toString()

            Glide
                .with(this)
                .load(it.display_name)
                .centerCrop()
                .placeholder(R.drawable.ic_account)
                .into(binding.circleImageView7);
         //Log.e("namenidhi", it.profile_image.toString())
        })



        setPreviewData()


    }

    private fun setPreviewData() {
        val currentImgPath=intent.getStringExtra(B_IMG_URL)
        try {
            val  mImageBitmap = BitmapFactory.decodeFile(currentImgPath) //MediaStore.Images.Media.getBitmap(this.contentResolver,Uri.parse(currentPhotoPath))
            binding.imgEvent.setImageBitmap(mImageBitmap)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        val record=intent.getSerializableExtra(EVE_RECORD) as EventsRecord
        val eve_date=record.startDate
        val start_time=record.startTime
        val end_time=record.endTime
        val location=record.location
        val title=record.title
        val link=record.linkOfEvent
        val about=record.description
      //  binding.textView125.text=about
        binding.textView119.text=location
        binding.textView115.text=title
        binding.textView121.text=record.languageName
        binding.textView125.text=about
        Log.e("lang","${record.languageName} $about")
        binding.textView117.text= start_time + " - " + end_time
        val type=record.type
        if (type.equals("1")){
            binding.textView108.text=getString(R.string.online_event)
            binding.textView118.text=getString(R.string.online)
        }else{
            binding.textView108.text=getString(R.string.local_event)
            binding.textView118.text=getString(R.string.local)
        }
        val simpleDateFormat = SimpleDateFormat("yyyy-M-dd", Locale.getDefault())
        try {
            val date1: Date = simpleDateFormat.parse(eve_date)
            val dd= SimpleDateFormat("EEE, dd MMM yyyy", Locale.getDefault())
            val ss:String=dd.format(date1)
            Log.e("fmtDate: ",ss)
            binding.textView116.text=ss
            val newDay=(ss.split(",")[1]).split(" ")[1]
            val newMonth=(ss.split(",")[1]).split(" ")[2]
            binding.textView76.text=newDay+" "+newMonth
        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("fmtDate: ",e.message.toString())
        }

    }

    override fun onBack() {
        onBackPressed()
    }

    override fun onPost() {
        /*val record=intent.getSerializableExtra(EVE_RECORD) as EventsRecord
        val currentImgPath=intent.getStringExtra(B_IMG_URL)
        val  bitmap= intent.getParcelableExtra<Bitmap>("image") as Bitmap
        binding.imgEvent.setImageBitmap(bitmap)*/
    }
}