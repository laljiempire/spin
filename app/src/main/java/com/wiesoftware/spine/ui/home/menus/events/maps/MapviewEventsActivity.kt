package com.wiesoftware.spine.ui.home.menus.events.maps

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.wiesoftware.spine.R
import com.wiesoftware.spine.RuntimeLocaleChanger
import com.wiesoftware.spine.data.adapter.EventContentAdapter
import com.wiesoftware.spine.data.net.reponses.EventsData
import com.wiesoftware.spine.data.net.reponses.EventsRecord
import com.wiesoftware.spine.data.repo.HomeRepositry
import com.wiesoftware.spine.databinding.ActivityMapviewEventsBinding
import com.wiesoftware.spine.ui.home.menus.events.B_IMG_URL
import com.wiesoftware.spine.ui.home.menus.events.EVE_RECORD
import com.wiesoftware.spine.ui.home.menus.events.IS_FROM_EVENT_DETAILS
import com.wiesoftware.spine.ui.home.menus.events.event_details.EventDetailActivity
import com.wiesoftware.spine.ui.home.menus.events.filter.FilterEventActivity
import com.wiesoftware.spine.ui.home.menus.spine.foryou.STORY_IMAGE
import com.wiesoftware.spine.util.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*
import kotlin.collections.ArrayList


class MapviewEventsActivity : AppCompatActivity(), OnMapReadyCallback,KodeinAware,
    MapviewEventListener, EventContentAdapter.SaveEventListener {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base?.let { RuntimeLocaleChanger.wrapContext(it) })
    }

    private val AUTOCOMPLETE_REQUEST_CODE = 1
    override val kodein by kodein()
    val factory: MapviewViewmodelFactory by instance()
    val homeRepositry: HomeRepositry by instance()
    lateinit var binding: ActivityMapviewEventsBinding
    var userId: String=""

    var lati:Double=0.0
    var longi:Double=0.0
    private lateinit var mMap: GoogleMap
    val PERMISSION_REQUEST_CODE = 94
    val permissions = arrayOf(
        android.Manifest.permission.ACCESS_FINE_LOCATION,
        android.Manifest.permission.ACCESS_COARSE_LOCATION,
        android.Manifest.permission.CAMERA,
        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
        android.Manifest.permission.READ_EXTERNAL_STORAGE
    )

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private var locationCallback: LocationCallback? = null
    private var allEvents: MutableList<EventsRecord> = ArrayList<EventsRecord>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil.setContentView(this, R.layout.activity_mapview_events)
       val viewmodel=ViewModelProvider(this, factory).get(MapviewViewmodel::class.java)
       binding.viewmodel=viewmodel
       viewmodel.mapviewEventListener=this

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest()

        viewmodel.getLoggedInUser().observe(this, Observer { user ->
            userId = user.users_id!!
            Log.e("userid",userId)
            getAllEvents()

            if (hasPermissions(this, permissions)){
                val IS_FROM_EVENT_DETAILS=intent.getBooleanExtra(IS_FROM_EVENT_DETAILS,false)
                if (!IS_FROM_EVENT_DETAILS){
                    getCurrentLocation()
                    getLocationUpdates()
                    startLocationUpdates()
                }
            }else{
                makeRequest()
            }
        })





        val apiKey = getString(R.string.google_maps_key)
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, apiKey)
        }
        val placesClient: PlacesClient = Places.createClient(this)
        val autocompleteFragment =
            supportFragmentManager.findFragmentById(R.id.autocomplete_fragment)
                    as AutocompleteSupportFragment
        autocompleteFragment.setPlaceFields(
            listOf(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.LAT_LNG
            )
        )
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
               // "Place: ${place.name}, ${place.id}".toast(this@MapviewEventsActivity)
                Log.e("searchPlace", "Place: ${place.name}, ${place.id}")
                val cameraPosition = CameraPosition.Builder()
                    .target(place.latLng)
                    .build()
                val cu = CameraUpdateFactory.newCameraPosition(cameraPosition)
                mMap.animateCamera(cu)
            }

            override fun onError(status: Status) {
               /* "An error occurred: $status".toast(this@MapviewEventsActivity)
                Log.e("searchPlace", "An error occurred: $status")*/
            }
        })


       /* binding.searchSpine.setOnQueryTextFocusChangeListener { v, hasFocus ->
            if (hasFocus){
                binding.searchSpine.visibility=View.INVISIBLE
                val autocompleteFragment =
                    supportFragmentManager.findFragmentById(R.id.autocomplete_fragment)
                            as AutocompleteSupportFragment
                autocompleteFragment.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME))
                autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
                    override fun onPlaceSelected(place: Place) {
                        Log.e("searchPlace", "Place: ${place.name}, ${place.id}")
                    }
                    override fun onError(status: Status) {
                        Log.e("searchPlace", "An error occurred: $status")
                    }
                })
            }
            binding.searchSpine.clearFocus()
        }*/


    }


    private fun getLocationUpdates()
    {
        locationRequest.interval = 50000
        locationRequest.fastestInterval = 50000
        locationRequest.smallestDisplacement = 170f // 170 m = 0.1 mile
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY //set according to your app function
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                if (locationResult.locations.isNotEmpty()) {
                    val location = locationResult.lastLocation
                    if (location != null){
                        lati=location.latitude
                        longi=location.longitude
                        Log.e("loc::", "$lati , $longi")
                        addCurrentMarker(lati, longi)

                        mMap?.let {
                            getAllEvents()
                        }
                    }
                }


            }
        }
    }
    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null /* Looper */
        )
    }
    private fun stopLocationUpdates() {
        if (locationCallback != null) {
            locationCallback?.let {
                fusedLocationClient.removeLocationUpdates(locationCallback)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        stopLocationUpdates()
    }

    override fun onResume() {
        super.onResume()
        if (!userId.isEmpty()) {
            val isFilter = Prefs.getBoolean("isFilter", false)
            if (isFilter) {
                val lat = Prefs.getString("lat", lati.toString())
                val date = Prefs.getString("date", "")
                val datetwo = Prefs.getString("datetwo", "")
                val category = Prefs.getString("category", "")
                val lon = Prefs.getString("lon", longi.toString())
                getFilteredList(lat, lon, date,datetwo, category)
            }
        }
    }
    private fun getFilteredList(
        lat: String?,
        lon: String?,
        date: String?,
        datetwo:String?,
        category: String?,

    ) {
        lifecycleScope.launch {
            try {
                val res=homeRepositry.getFilteredEventList(1,100,userId,lat!!,lon!!,10,date!!,datetwo!!,category!!)
                allEvents.clear()
                if (res.status){
                    STORY_IMAGE=res.image
                    val dataList=res.data
                    setEventsOnMap(dataList)
                    Log.e("filteredRes: ",""+dataList)

                }else{
                    "${res.message}".toast(this@MapviewEventsActivity)

                }
                Prefs.putAny("isFilter",false)
            }catch (e: ApiException){
                e.printStackTrace()
                Prefs.putAny("isFilter",false)
            }catch (e: NoInternetException){
                e.printStackTrace()
                Prefs.putAny("isFilter",false)
            }
        }
    }

    fun hasPermissions(context: Context, permissions: Array<String>): Boolean{
        for(p in permissions){
            if(ActivityCompat.checkSelfPermission(context, p) != PackageManager.PERMISSION_GRANTED){
                return false
            }
        }
        return true
    }
    fun makeRequest() {
        ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
    }

    private fun getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null){
                    lati=location.latitude
                    longi=location.longitude

                    addCurrentMarker(lati, longi)
                    mMap?.let {
                        getAllEvents()
                    }
                }
            }
    }

    private fun addCurrentMarker(lati: Double, longi: Double) {
        val latLng=LatLng(lati, longi)
        val marker: MarkerOptions=MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.defaultMarker()).title(
            "Current position"
        )
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f))
        mMap.addMarker(marker)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val IS_FROM_EVENT_DETAILS=intent.getBooleanExtra(IS_FROM_EVENT_DETAILS,false)
        if (IS_FROM_EVENT_DETAILS){
            val record=intent.getSerializableExtra(EVE_RECORD) as EventsRecord
            try {
            val latitude=record.latitude.toDouble()
            val longitude=record.longitude.toDouble()
            val latlng=LatLng(latitude,longitude)
                val title=record.title
                val fee=/*record.feeCurrency+" "+*/record.fee
                val symbol=record.symbol
            addMarker(fee, title, latlng, -1,symbol)


                val cameraPosition = CameraPosition.Builder()
                    .target(latlng)
                    .build()
                val cu = CameraUpdateFactory.newCameraPosition(cameraPosition)
                mMap.animateCamera(cu)
                return
            }catch (e: Exception){
                e.printStackTrace()
            }

        }

    }

    private fun getAllEvents() {
        lifecycleScope.launch {
            try {
                val res=homeRepositry.getAllEvents(1, 100, userId,"localevent")//homeRepositry.getNearbyEvents(1,100,userId,lati,longi,10)
                if (res.status){
                    STORY_IMAGE =res.image
                    val dataList=res.data
                   setEventsOnMap(dataList)
                }
            }catch (e: ApiException){
                e.printStackTrace()
            }catch (e: NoInternetException){
                e.printStackTrace()
            }
        }
    }

    private fun setEventsOnMap(dataList: List<EventsData>) {
        mMap.clear()
        var k=0
        for (i in dataList.indices){
            val record=dataList[i].records
            for (j in record.indices){
                val data=record[j]
                allEvents.add(data)
                val lat=data.latitude
                val lon=data.longitude
                val title=data.title
                val fee=/*data.feeCurrency+" "+*/data.fee
                var symbol=data.symbol
                if (symbol == null){
                    symbol="$"
                }
                lat?.let {
                    try {
                        val l: Double=lat.toDouble()
                        val ll: Double=lon.toDouble()
                        Log.e("latlongs:", lat + " " + lon)
                        addMarker(fee, title, LatLng(l, ll), k,symbol)
                    }catch (e: Exception){
                        e.printStackTrace()
                        Log.e("latlongssss:", lat + " " + lon)
                        getCurrentLocation()
                        addMarker(fee, title, LatLng(lati, longi), k,symbol)
                    }
                }
                k++
            }
        }
        setEveList()
    }

    private fun setEveList() {
        binding.rvMapeve.also {
            it.layoutManager=LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
            it.setHasFixedSize(true)
            it.adapter=EventContentAdapter(this, allEvents, this)
        }
    }

    fun addMarker(price: String, title: String, latLng: LatLng, k: Int, symbol: String) {
        val markerView = View.inflate(this, R.layout.marker_view, null)
        val tv: TextView=markerView.findViewById(R.id.textView173) as TextView
        tv.text=symbol+" "+price
        val bitmap: Bitmap=getBitmapFromView(markerView)
        val marker: MarkerOptions=MarkerOptions().position(latLng).icon(
            BitmapDescriptorFactory.fromBitmap(
                bitmap
            )
        ).title(title)
        mMap.addMarker(marker).tag=k
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,16.0f))
        mMap.setOnInfoWindowClickListener { marker->
            try {
                val pos: Int= marker.tag as Int
                showEvents(pos)
            }catch (e: Exception){
                e.printStackTrace()
            }

        }
    }
    fun showEvents(pos: Int) {
        binding.rvMapeve.postDelayed(Runnable {
            binding.rvMapeve.smoothScrollToPosition(pos)
        }, 1000)
    }


    override fun onBack() {
        onBackPressed()
    }

    private fun getBitmapFromView(view: View): Bitmap {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        val bitmap = Bitmap.createBitmap(
            view.measuredWidth,
            view.measuredHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        view.layout(0, 0, view.measuredWidth, view.measuredHeight)
        view.draw(canvas)
        return bitmap
    }

    override fun onFilter() {
        startActivity(Intent(this, FilterEventActivity::class.java))
    }

    override fun onEventSaved(record: EventsRecord,value: Int) {
        lifecycleScope.launch {
            try {
                val res=homeRepositry.saveEvents(userId, record.id)
                if (res.status){
                    val msg=res.message
                    msg.toast(this@MapviewEventsActivity)

                }
            }catch (e: ApiException){
                e.printStackTrace()
            }catch (e: NoInternetException){
                e.printStackTrace()
            }
        }
    }



    override fun onEventDetails(record: EventsRecord) {
        val intent=Intent(this, EventDetailActivity::class.java)
        intent.putExtra(EVE_RECORD, record)
        intent.putExtra(B_IMG_URL, STORY_IMAGE)
        intent.putExtra("event_id",record.id)
        startActivity(intent)
    }



}