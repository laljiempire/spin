package com.wiesoftware.spine.ui.home.menus.profile.setting.help.privacy

/**
 * Created by Vivek kumar on 2/22/2021.
 * Email: vivekpcst.kumar@gmail.com.
 */
interface PrivacySettingEventListener {
    fun onBack()
    fun onPrivacyPolicy()
}