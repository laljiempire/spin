package com.wiesoftware.spine.ui.home.menus.profile.setting.my_ads

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.wiesoftware.spine.R
import com.wiesoftware.spine.RuntimeLocaleChanger
import com.wiesoftware.spine.data.repo.HomeRepositry
import com.wiesoftware.spine.databinding.ActivityMyAdsBinding
import com.wiesoftware.spine.ui.home.menus.profile.myprofile.MyProfileEventListener
import kotlinx.coroutines.launch
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MyAdsActivity : AppCompatActivity(),KodeinAware, MyAdsEventListener {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base?.let { RuntimeLocaleChanger.wrapContext(it) })
    }

    override val kodein by kodein()
    lateinit var binding: ActivityMyAdsBinding
    val homeRepositry: HomeRepositry by instance()
    lateinit var userId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_my_ads)
        val viewModel = ViewModelProvider(this).get(MyAdsViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.myAdsEventListener = this
        homeRepositry.getUser().observe(this, Observer { user->
            userId = user.users_id!!
            getMyAds()
        })
    }

    private fun getMyAds() {
        lifecycleScope.launch {
            try {

            }catch (e: Exception){
                e.printStackTrace()
            }
        }
    }

    override fun onBack() {
        onBackPressed()
    }
}