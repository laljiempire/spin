package com.wiesoftware.spine.ui.auth.number

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.wiesoftware.spine.R
import com.wiesoftware.spine.data.db.entities.User
import com.wiesoftware.spine.data.repo.AuthRepositry
import com.wiesoftware.spine.databinding.ActivityNumberBinding
import com.wiesoftware.spine.ui.auth.otp.OtpActivity
import com.wiesoftware.spine.ui.auth.otp.user_id
import com.wiesoftware.spine.ui.auth.register.RegisterActivity
import com.wiesoftware.spine.util.*
import kotlinx.android.synthetic.main.activity_number.*
import org.kodein.di.android.kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class NumberActivity : AppCompatActivity(), NumberEventListener, KodeinAware {

    override val kodein by kodein()
    private val factory: NumberViewModelFactory by instance()
    private val authRepositry: AuthRepositry by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityNumberBinding=DataBindingUtil.setContentView(this,R.layout.activity_number)
        val viewModel=ViewModelProvider(this,factory).get(NumberViewModel::class.java)
        binding.viewmodel=viewModel
        viewModel.numberEventListener=this
        val userRes=intent.getSerializableExtra(RegisterActivity.REGISTERED_USER) as User
        user_id=userRes.users_id!!
    }

    override fun onBack() {
        onBackPressed()
    }

    override fun onNext(phoneNumber: String?) {
        pbNumber.show()

        Coroutines.main {
            try {
                val response = authRepositry.sendCode(user_id, phoneNumber!!)
                if (response.status!!){
                    response.data?.let {
                        //authRepositry.saveUser(it)
                        toast(it.verification_pin!!)
                        val intent=Intent(this, OtpActivity::class.java)
                        intent.putExtra(RegisterActivity.REGISTERED_USER,it)
                        intent.putExtra("phone",phoneNumber)
                        startActivity(intent)
                    }
                }else{
                    toast(response.message!!)
                }
                pbNumber.hide()
            }catch (e:ApiException){
                toast(e.message!!)
                pbNumber.hide()
            }catch (e:NoInternetException){
                toast(e.message!!)
                pbNumber.hide()
            }
        }
    }

    override fun onCodeFailed(msg: String?) {
        toast(msg!!)
    }

}