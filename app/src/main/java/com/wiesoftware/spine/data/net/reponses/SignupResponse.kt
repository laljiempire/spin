package com.wiesoftware.spine.data.net.reponses

import com.wiesoftware.spine.data.db.entities.User

data class SignupResponse(
    var status: Boolean,
    var data: User,
    var message: String
)