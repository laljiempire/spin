package com.wiesoftware.spine.data.net.reponses

data class AdDurationRes(
    val `data`: List<AdDurationData>,
    val message: String,
    val status: Boolean
)